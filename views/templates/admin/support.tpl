{*
* NOTICE OF LICENSE
*
* Aqurate - Integration for PrestaShop.
* @author MACHINE LEARNING SOLUTIONS SRL
* @copyright Copyright (C) 2023 MACHINE LEARNING SOLUTIONS SRL
* @license https://opensource.org/license/mit The MIT License (MIT) 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
* THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*}

<div class="panel">


    <div class="panel-heading">
        <i class="icon-support"></i> {l s='Prerequisites and support' mod='aqurate'}
    </div>
	
	<div class="moduleconfig-content">
		<div class="row">
		   <div class="col-xs-12">
		   
               <p>{l s='In order for this module to work, you need to make sure of the following' mod='aqurate'}:</p>
               
               <p> - {l s='You have an Aqurate account with a trial or active Aqurate Personalize subscription. Get it' mod='aqurate'} <a href="https://app.aqurate.ai" target="_blank">{l s='here' mod='aqurate'}</a>.</p>
               <p> - {l s='You have' mod='aqurate'} <a href="https://aqurate.ai/knowledge" target="_blank">{l s='connected PrestaShop' mod='aqurate'}</a> {l s='with Aqurate to enable the data flow' mod='aqurate'}.</p>
               <p> - {l s='You have correctly installed the native PrestaShop "Cross-selling" and "Products in the same category" widgets, and they are working on your shop’s page' mod='aqurate'}.</p> 
               
               <p>{l s='Spotted a bug, need some help, or just wanted to say hi' mod='aqurate'}?</p>
               
               <p><a href="https://api.aqurate.ai/v1/docs" target="_blank">{l s='API Reference' mod='aqurate'}</a></p>
               
               <p><a href="https://www.aqurate.ai/knowledge" target="_blank">{l s='Knowledge base' mod='aqurate'}</a></p>
               
               <p><a href="mailto:support@aqurate.ai" target="_blank">{l s='Get in touch directly' mod='aqurate'}</a></p>

			
            </div>			
	    </div>

   </div>   
      
      
</div>
		       