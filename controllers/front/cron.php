<?php
/**
* NOTICE OF LICENSE
*
* Aqurate - Integration for PrestaShop.
* @author MACHINE LEARNING SOLUTIONS SRL
* @copyright Copyright (C) 2023 MACHINE LEARNING SOLUTIONS SRL
* @license https://opensource.org/license/mit The MIT License (MIT) 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
* THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

class AqurateCronModuleFrontController extends ModuleFrontController
{
    
    public function __construct()
    {
        
        $token = Tools::getValue('token');
        if (!Tools::getValue('token') || empty($token)
            || !Tools::getValue('key')
            || md5(_COOKIE_KEY_ . Tools::getValue('key')) != $token
        ) {
            die('Bad token!');
        }
        
        parent::__construct();
        
    }

    
    public function initContent()
    {
        
        die('The cron function was deactivated. It is no longer needed!');
        
        $output = '';
        
        if (!Configuration::get('AQURATE_USERNAME') or !Configuration::get('AQURATE_PASS') or !Module::isInstalled('aqurate') or !Module::isEnabled('aqurate')) return;
        
        if (!Tools::getValue('method')) die('Missing API function from url!');
        
        if ($newtoken = $this->module->clasa->getToken()) {
            
            $callingfunction = 'collect'.ucfirst((string)Tools::getValue('method'));
            
            $res = $this->module->clasa->$callingfunction($newtoken);
            
            sleep(20);
            
            while (true) {
                
                $res = $this->module->clasa->$callingfunction($newtoken, $res);
                
                sleep(20);
                
                if (Tools::strpos($res,'&marker') === false) {
                    break;
                }
                
            }

            die('Cron executed succesfully!');
            
        } else die('Invalid API token!');
        
    }
    

    
}

    
    
?>