<?php
/**
* NOTICE OF LICENSE
*
* Aqurate - Integration for PrestaShop.
* @author MACHINE LEARNING SOLUTIONS SRL
* @copyright Copyright (C) 2023 MACHINE LEARNING SOLUTIONS SRL
* @license https://opensource.org/license/mit The MIT License (MIT) 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
* THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once _PS_MODULE_DIR_ . 'aqurate/classes/Recommendations.php';

class Aqurate extends Module {
    
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'aqurate';
        $this->tab = 'front_office_features';
        $this->module_key= '673e28ce43d027e09d6ed745ec51389f';
        $this->version = '1.2.0';
        $this->author = 'aqurate.ai';
        $this->need_instance = 1;

        $this->bootstrap = true;
        

        parent::__construct();
        
        $this->clasa = new Recommendations();
        
        $this->aqurate_version = 'prestashop-1.7-'.$this->version;
        
        $this->secure_key = Tools::encrypt($this->name);
        $this->secure_iv = '';
        $this->secure_tag = '';
        
        $this->displayName = $this->l('Aqurate');
        $this->description = $this->l('This module allows connects you to Aqurate Recommendations');
        

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall this module?');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }
    

    public function install()
    {
        
        $dependecy_module = Module::isInstalled('ps_categoryproducts');
        if (!$dependecy_module)
            $dependecy_module = Module::isInstalled('ps_crossselling');
        
        if (!$dependecy_module) {
            $this->_errors[] = Context::getContext()->getTranslator()->trans('Unable to install the module because neither ps_categoryproducts or ps_crossselling are not installed on this shop.', array(), 'Admin.Modules.Notification');
            return false;    
        }
        
        
        include(dirname(__FILE__).'/sql/install.php');
    
        return parent::install() && $this->registerHook('actionAdminControllerSetMedia');
        
    }

    public function uninstall()
    {
       
        include(dirname(__FILE__).'/sql/uninstall.php');
        
        return parent::uninstall();
    }
    
    
    public function getContent()
    {
    
        $this->_html='';   
        
        
        $pc_module = Module::isInstalled('ps_categoryproducts');
        if (!$pc_module)
            $this->_html.=$this->displayWarning($this->l('ps_categoryproducts module is not installed on your shop. Similar Products won\'t be displayed on product page. To fix these please install the module and then reset this module.'));   
        
        $pc_module = Module::isEnabled('ps_categoryproducts');
        if (!$pc_module)
            $this->_html.=$this->displayWarning($this->l('ps_categoryproducts module is not enabled on your shop. Similar Products won\'t be displayed on product page. To fix these please install the module and then reset this module.')); 
        
        $cs_module = Module::isInstalled('ps_crossselling');
        if (!$cs_module)
            $this->_html.=$this->displayWarning($this->l('ps_crossselling module is not installed on your shop. Cross Selling Products won\'t be displayed on product page. To fix these please install the module and then reset this module.'));   
        
        $cs_module = Module::isEnabled('ps_crossselling');
        if (!$pc_module)
            $this->_html.=$this->displayWarning($this->l('ps_crossselling module is not enabled on your shop. Cross Selling Products won\'t be displayed on product page. To fix these please install the module and then reset this module.'));   

        
       
        if (((bool)Tools::isSubmit('submitLot_Module')) == true) {
         if ($this->postValidation())    
            $this->postProcess();
        }
        
        
        $this->context->smarty->assign('module_dir', $this->_path);
        

        $this->_html .= $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');
        
        $this->_html .= $this->renderForm('credentials');
        $this->_html .= $this->renderForm('pdp');
        $this->_html .= $this->renderForm('cart');
        $this->_html .= $this->renderForm('cartpopup');
        
        /*
        $this->_html .= $this->displayWarning($this->l('Do not forget to add the following link to cron jobs. ').'<br /><br />'.$this->context->link->getModuleLink($this->name, 'cron', array('method' => 'crossseling', 'key' => $this->secure_key, 'token' => md5(_COOKIE_KEY_ . $this->secure_key))).'<br /><br />'.$this->context->link->getModuleLink($this->name, 'cron', array('method' => 'substitute', 'key' => $this->secure_key, 'token' => md5(_COOKIE_KEY_ . $this->secure_key))).'<br /><br />'. $this->context->link->getModuleLink($this->name, 'cron', array('method' => 'similar', 'key' => $this->secure_key, 'token' => md5(_COOKIE_KEY_ . $this->secure_key))).'<br />');
        */
        
         $this->_html .= $this->context->smarty->fetch($this->local_path.'views/templates/admin/support.tpl');
        //$this->_html .= $this->renderSuport();
        
        
        
        return $this->_html;
   
    }
   
    
    
    protected function renderForm($block_name)
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitLot_Module';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        
       
        
         
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), 
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );
        
        

        return $helper->generateForm(array($this->getConfigForm($block_name)));
        
    }
    
    

   
    protected function getConfigForm($block_name)        
    {
        
        switch ($block_name) {
            case 'credentials':    
        
        $form_output = array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Aqurate credentials'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                   
                    /*
                    array(
                        'type' => 'text',
                        'name' => 'AQURATE_USERNAME',
                        'label' => $this->l('Aqurate Username')
                        ),
                    
                    array(
                        'type' => 'text',
                        'name' => 'AQURATE_PASS',
                        'label' => $this->l('Aqurate Password')
                        ),
                    */    
                    array(
                        'type' => 'text',
                        'name' => 'AQURATE_TOKEN',
                        'label' => $this->l('Aqurate API Token'),
                        'class' => 'token'
                        ),
   
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
        break;        
                
            case 'pdp':
                
                $form_output = array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Recommendations on your product detail page'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                
                     array(
                        'type' => 'switch',
                        'label' => $this->l('Cross-sell Recommendations in the "Cross-selling" widget'),
                        'name' => 'AQURATE_CROSSSELLING_PRODUCT',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Substitute Recommendations in the “Products in the same category” widget'),
                        'name' => 'AQURATE_SUBSTITUTE_PRODUCT',
                        'class' => 'swicth_exclusiv_1',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Similar Recommendations in the “Products in the same category” widget'),
                        'name' => 'AQURATE_SIMILAR_PRODUCT',
                        'class' => 'swicth_exclusiv_1',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
   
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
        break;        
                
        case 'cart':
                
                $form_output = array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Recommendations on your cart page'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                
                     array(
                        'type' => 'switch',
                        'label' => $this->l('Cross-sell Recommendations in the “Cross-selling” widget'),
                        'name' => 'AQURATE_CROSSSELLING_CART',
                        'is_bool' => true,
                        'class' => 'swicth_exclusiv_2',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Similar Recommendations in the “Cross-selling” widget'),
                        'name' => 'AQURATE_SIMILAR_CART',
                        'is_bool' => true,
                        'class' => 'swicth_exclusiv_2',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );        
        break;
                
        case 'cartpopup':
                
                $form_output = array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Recommendations on your add to cart popup'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                
                     array(
                        'type' => 'switch',
                        'label' => $this->l('Cross-sell Recommendations in the “Cross-selling” widget'),
                        'name' => 'AQURATE_CROSSSELLING_CARTPOPUP',
                        'is_bool' => true,
                        'class' => 'swicth_exclusiv_3',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Similar Recommendations in the “Cross-selling” widget'),
                        'name' => 'AQURATE_SIMILAR_CARTPOPUP',
                        'is_bool' => true,
                        'class' => 'swicth_exclusiv_3',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );        
        break;        
        }
        
        return $form_output;
                
    }
    
    
     protected function getConfigFormValues() {

        $password = $this->clasa->decryptPass(Configuration::get('AQURATE_PASS'));
         
        return array(
            //'AQURATE_USERNAME' => Configuration::get('AQURATE_USERNAME'),
            //'AQURATE_PASS' => $password,
            'AQURATE_TOKEN' => Configuration::get('AQURATE_TOKEN'),
            'AQURATE_CROSSSELLING_PRODUCT' => Configuration::get('AQURATE_CROSSSELLING_PRODUCT'),
            'AQURATE_SIMILAR_PRODUCT' => Configuration::get('AQURATE_SIMILAR_PRODUCT'),
            'AQURATE_SUBSTITUTE_PRODUCT' => Configuration::get('AQURATE_SUBSTITUTE_PRODUCT'),
            'AQURATE_SIMILAR_CART' => Configuration::get('AQURATE_SIMILAR_CART'),
            'AQURATE_CROSSSELLING_CART' => Configuration::get('AQURATE_CROSSSELLING_CART'),
            'AQURATE_SIMILAR_CARTPOPUP' => Configuration::get('AQURATE_SIMILAR_CARTPOPUP'),
            'AQURATE_CROSSSELLING_CARTPOPUP' => Configuration::get('AQURATE_CROSSSELLING_CARTPOPUP'),
        );
        
        
        
    }
    
    
    protected function postValidation() {
        
        return true;
        
    }
    
    
    
    
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();
        
        foreach (array_keys($form_values) as $key) {
            
            if (!Tools::isSubmit($key)) continue;
            
            if (strpos('x'.$key,'AQURATE_PASS') > 0) {
                
                if ($encrypted_password = $this->clasa->encryptPass(Tools::getValue($key)))
                    Configuration::updateValue($key, $encrypted_password);
                else Configuration::updateValue($key, Tools::getValue($key));
                
           } else {        
                Configuration::updateValue($key, Tools::getValue($key));
            }
            
        }
        
        
        $this->_html.=$this->displayConfirmation($this->l('Settings updated!'));   
        
    }
    


    public function hookactionAdminControllerSetMedia($params){
        
        if (Tools::getValue('configure') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
  
	}
    
    
    

 
}
