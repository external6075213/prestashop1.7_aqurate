<?php
/**
* NOTICE OF LICENSE
*
* Aqurate - Integration for PrestaShop.
* @author MACHINE LEARNING SOLUTIONS SRL
* @copyright Copyright (C) 2023 MACHINE LEARNING SOLUTIONS SRL
* @license https://opensource.org/license/mit The MIT License (MIT) 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
* THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Recommendations extends Module {
    
    const ApiUrl = 'https://api.aqurate.ai/v1/';


    public function getToken() {
        
        
        if (Configuration::get('AQURATE_API_TOKEN_EXPIRE')) {

            if (time() < strtotime(Configuration::get('AQURATE_API_TOKEN_EXPIRE')))

                return Configuration::get('AQURATE_API_TOKEN');

        } 

        $username = Configuration::get('AQURATE_USERNAME');

        $password = $this->decryptPass(Configuration::get('AQURATE_PASS'));

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, self::ApiUrl.'account/login');
        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'email='.$username.'&password='.$password);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $response = json_decode($response);
        if (!$response) { 
            $this->addConnectionLog('getToken', 'Could not connect to ERP API');
            return false;
        }

        if (isset($response->token->X_Auth_Token)) {
            Configuration::updateValue('AQURATE_API_TOKEN_EXPIRE', date('Y-m-d H:i:s', time()+$response->token->expires_in));
            Configuration::updateValue('AQURATE_API_TOKEN', $response->token->X_Auth_Token);
            return $response->token->X_Auth_Token;
        } else { 
            if (isset($response->detail))
                $this->addConnectionLog('getToken', $response->detail);
            else  $this->addConnectionLog('getToken', 'Could not connect to ERP API (unknown)');
            return false; 
        }

    }
    
    
    public function collectSimilar($id_product) {
        
        $token = Configuration::get('AQURATE_TOKEN');
        if (!$token) return false;
        
        //$get_variables = '?email='.Configuration::get('AQURATE_USERNAME').'&top=40&recs_no=40&limit=300';
        
        $ch = curl_init();
        /*
        if ($custom_api_url != '') {
        $custom_api_url=str_replace('&limit=1000','&limit=300',$custom_api_url);      
        $custom_api_url = urldecode($custom_api_url);  
        curl_setopt($ch, CURLOPT_URL, $custom_api_url);  
        }   
        else
        */
        
        $headers = $this->getAPICallHeaders();
        
        
        curl_setopt($ch, CURLOPT_URL, self::ApiUrl.'recs/item/similar?product_id='.$id_product.'&top=40');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $response = json_decode($response);

        if (!$response) { 
                $this->addConnectionLog('collectSimilar', 'Could not connect to ERP API');
                return false;
        }

        if (isset($response->recs)) {
            
            //$this->addConnectionLog('collectSimilar', 'success');
            
            /*
            if (isset($response->links->next)) {
                $next_call_url = $response->links->next;
                Db::getInstance()->Execute("INSERT INTO "._DB_PREFIX_."aqurate_cron_jobs (type, action, date_add) VALUES ('collectSimilar','".pSQL($response->links->next)."',NOW())");
            } else {
                Db::getInstance()->Execute("INSERT INTO "._DB_PREFIX_."aqurate_cron_jobs (type, action, date_add) VALUES ('collectSimilar','gata',NOW())");
                $next_call_url = '';
            }
            */
            
            
            $date_upd = date('Y-m-d H:i:s', time());
            
            $result = array();    
            
            foreach ($response->recs as $rec) {
                
                $search = Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."aqurate_similar WHERE id_product='".pSQL($rec->product_id)."' AND id_similar_product='".pSQL($rec->rec_product_id)."'");
                
                if ($search) {
                    
                    Db::getInstance()->Execute("UPDATE "._DB_PREFIX_."aqurate_similar SET position='".pSQL($rec->position)."', date_upd='".pSQL($date_upd)."' WHERE id_product='".pSQL($rec->product_id)."' AND id_similar_product='".pSQL($rec->rec_product_id)."'");
                    
                } else {
                    
                    Db::getInstance()->Execute("INSERT INTO "._DB_PREFIX_."aqurate_similar (id_product, id_similar_product, position, date_upd) VALUES ('".pSQL($rec->product_id)."','".pSQL($rec->rec_product_id)."', '".pSQL($rec->position)."', '".pSQL($date_upd)."')");
                }
                
                $result[] = $rec->rec_product_id;
                
            }
            
            return $result;
            
        } else { 
                if (isset($response->detail))
                    $this->addConnectionLog('collectSimilar', json_encode($response));
                else  $this->addConnectionLog('collectSimilar', 'Could not connect to ERP API (unknown)');
                return false; 
            }
        
        return false;
        
    }
    
    
    public function collectSubstitute($id_product) {
        
        
        $token = Configuration::get('AQURATE_TOKEN');
        if (!$token) return false;
        
        //$get_variables = '?email='.Configuration::get('AQURATE_USERNAME').'&top=40&recs_no=40&limit=300';

        $ch = curl_init();
        /*
        if ($custom_api_url != '') {
        $custom_api_url=str_replace('&limit=1000','&limit=300',$custom_api_url);    
        $custom_api_url = urldecode($custom_api_url);  
        curl_setopt($ch, CURLOPT_URL, $custom_api_url);  
        }
        else 
        */
        
        $headers = $this->getAPICallHeaders();
        
        curl_setopt($ch, CURLOPT_URL, self::ApiUrl.'recs/item/substitutes?product_id='.$id_product.'&top=40');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $response = json_decode($response);

        if (!$response) { 
                $this->addConnectionLog('collectSubstitute', 'Could not connect to ERP API');
                return false;
        }

        if (isset($response->recs)) {
            
            //$this->addConnectionLog('collectSimilar', 'success');
            /*
            if (isset($response->links->next)) {
                $next_call_url = $response->links->next;
                //Db::getInstance()->Execute("INSERT INTO "._DB_PREFIX_."aqurate_cron_jobs (type, action, date_add) VALUES ('collectSubstitute','".pSQL($response->links->next)."',NOW())");
            } else {
                $next_call_url = '';
            }
            */
            $date_upd = date('Y-m-d H:i:s', time());
            $result = array();
            
            foreach ($response->recs as $rec) {
                
                $search = Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."aqurate_substitute WHERE id_product='".pSQL($rec->product_id)."' AND id_substitute_product='".pSQL($rec->rec_product_id)."'");
                
                if ($search) {
                    
                    Db::getInstance()->Execute("UPDATE "._DB_PREFIX_."aqurate_substitute SET position='".pSQL($rec->position)."', date_upd='".pSQL($date_upd)."' WHERE id_product='".pSQL($rec->product_id)."' AND id_substitute_product='".pSQL($rec->rec_product_id)."'");
                    
                } else {
                    
                    Db::getInstance()->Execute("INSERT INTO "._DB_PREFIX_."aqurate_substitute (id_product, id_substitute_product, position, date_upd) VALUES ('".pSQL($rec->product_id)."','".pSQL($rec->rec_product_id)."', '".pSQL($rec->position)."', '".pSQL($date_upd)."')");
                }
                
                $result[] = $rec->rec_product_id;
                
            }
            

            return $result;
            
        } else { 
                if (isset($response->detail))
                    $this->addConnectionLog('collectSubstitute', json_encode($response));
                else  $this->addConnectionLog('collectSubstitute', 'Could not connect to ERP API (unknown)');
                return false; 
            }
        
        return false;
        
    }
    
    public function collectCrossseling($id_product) {
        
        //$get_variables = '?email='.Configuration::get('AQURATE_USERNAME').'&top=40&recs_no=40&limit=300';
        
        $token = Configuration::get('AQURATE_TOKEN');
        if (!$token) return false;

        $ch = curl_init();
        /*
        if ($custom_api_url != '') {
        $custom_api_url=str_replace('&limit=1000','&limit=300',$custom_api_url);       
        $custom_api_url = urldecode($custom_api_url);  
        curl_setopt($ch, CURLOPT_URL, $custom_api_url);  
        }
        else  
        */
        
        $headers = $this->getAPICallHeaders();
        
        curl_setopt($ch, CURLOPT_URL, self::ApiUrl.'recs/item/cross-sell?product_id='.$id_product.'&top=40');

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($response);
        
        if (!$response) { 
                $this->addConnectionLog('collectCrossseling', 'Could not connect to ERP API: '.json_encode($response));
                return false;
        }

        if (isset($response->recs)) {
            
            /*
            if (isset($response->links->next)) {
                $next_call_url = $response->links->next;
                //Db::getInstance()->Execute("INSERT INTO "._DB_PREFIX_."aqurate_cron_jobs (type, action, date_add) VALUES ('collectCrossseling','".pSQL($response->links->next)."',NOW())");
            } else {
                $next_call_url = '';
            }
            */
            
            $date_upd = date('Y-m-d H:i:s', time());
            
            $result = array();
            
            foreach ($response->recs as $rec) {
                
                $search = Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."aqurate_crossselling WHERE id_product='".pSQL($rec->product_id)."' AND id_cross_product='".pSQL($rec->rec_product_id)."'");
                
                if ($search) {
                    
                    Db::getInstance()->Execute("UPDATE "._DB_PREFIX_."aqurate_crossselling SET position='".pSQL($rec->position)."', date_upd='".pSQL($date_upd)."' WHERE id_product='".pSQL($rec->product_id)."' AND id_cross_product='".pSQL($rec->rec_product_id)."'");
                    
                } else {
                    
                    Db::getInstance()->Execute("INSERT INTO "._DB_PREFIX_."aqurate_crossselling (id_product, id_cross_product, position, date_upd) VALUES ('".pSQL($rec->product_id)."','".pSQL($rec->rec_product_id)."', '".pSQL($rec->position)."', '".pSQL($date_upd)."')");
                }
                
                $result[] = $rec->rec_product_id;
                
                
            }
            
            return $result;
            
        } else { 
                if (isset($response->detail))
                    $this->addConnectionLog('collectCrossseling', json_encode($response));
                else  $this->addConnectionLog('collectCrossseling', 'Could not connect to ERP API (unknown)');
                return false; 
            }
        
        return false;
    }
    
    
    
    public function getSimilarProducts($id_product) {
     
        $yesterday = date('Y-m-d H:i:s', time() - 24*3600); 
        
        $product_ids = Db::getInstance()->ExecuteS("SELECT id_similar_product FROM "._DB_PREFIX_."aqurate_similar WHERE id_product='".pSQL($id_product)."' AND date_upd>'".pSQL($yesterday)."' ORDER BY position ASC");
        
        if (!$product_ids) {
            $product_ids = $this->collectSimilar($id_product);
            if (!$product_ids) return false;
            return $product_ids;
        }
        else return array_column($product_ids, 'id_similar_product');
        
    }
    
    
    public function getSubstituteProducts($id_product) {
        
        $yesterday = date('Y-m-d H:i:s', time() - 24*3600); 
        
        $product_ids = Db::getInstance()->ExecuteS("SELECT id_substitute_product FROM "._DB_PREFIX_."aqurate_substitute WHERE id_product='".pSQL($id_product)."' AND date_upd>'".pSQL($yesterday)."' ORDER BY position ASC");
        
        if (!$product_ids) {
            $product_ids = $this->collectSubstitute($id_product);
            if (!$product_ids) return false;
            return $product_ids;
        }
        else return array_column($product_ids, 'id_substitute_product');
        
    }
    
    
    public function getCrossSellingProducts($id_product) {
         
        $yesterday = date('Y-m-d H:i:s', time() - 24*3600); 
        
        $product_ids = Db::getInstance()->ExecuteS("SELECT id_cross_product FROM "._DB_PREFIX_."aqurate_crossselling WHERE id_product='".pSQL($id_product)."' AND date_upd>'".pSQL($yesterday)."' ORDER BY position ASC");
        
        if (!$product_ids) {
            $product_ids = $this->collectCrossseling($id_product);
            if (!$product_ids) return false;
            return $product_ids;
        }
        else return array_column($product_ids, 'id_cross_product');
       
        
    }
    
    
    
    public function getProductDetailsAsArray($id_product, $active = true, $front = true) {
        
        $nbDaysNewProduct = Configuration::get('PS_NB_DAYS_NEW_PRODUCT');
        if (!Validate::isUnsignedInt($nbDaysNewProduct)) {
            $nbDaysNewProduct = 20;
        }
        
        $idLang = $this->context->language->id;

        $sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) AS quantity' . (Combination::isFeatureActive() ? ', IFNULL(product_attribute_shop.id_product_attribute, 0) AS id_product_attribute,
					product_attribute_shop.minimal_quantity AS product_attribute_minimal_quantity' : '') . ', pl.`description`, pl.`description_short`, pl.`available_now`,
					pl.`available_later`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, image_shop.`id_image` id_image,
					il.`legend` as legend, m.`name` AS manufacturer_name, cl.`name` AS category_default,
					DATEDIFF(product_shop.`date_add`, DATE_SUB("' . date('Y-m-d') . ' 00:00:00",
					INTERVAL ' . (int) $nbDaysNewProduct . ' DAY)) > 0 AS new, product_shop.price AS orderprice
				FROM `' . _DB_PREFIX_ . 'category_product` cp
				LEFT JOIN `' . _DB_PREFIX_ . 'product` p
					ON p.`id_product` = cp.`id_product`
				' . Shop::addSqlAssociation('product', 'p') .
                (Combination::isFeatureActive() ? ' LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute_shop` product_attribute_shop
				ON (p.`id_product` = product_attribute_shop.`id_product` AND product_attribute_shop.`default_on` = 1 AND product_attribute_shop.id_shop=' . (int) $this->context->shop->id . ')' : '') . '
				' . Product::sqlStock('p', 0) . '
				LEFT JOIN `' . _DB_PREFIX_ . 'category_lang` cl
					ON (product_shop.`id_category_default` = cl.`id_category`
					AND cl.`id_lang` = ' . (int) $idLang . Shop::addSqlRestrictionOnLang('cl') . ')
				LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl
					ON (p.`id_product` = pl.`id_product`
					AND pl.`id_lang` = ' . (int) $idLang . Shop::addSqlRestrictionOnLang('pl') . ')
				LEFT JOIN `' . _DB_PREFIX_ . 'image_shop` image_shop
					ON (image_shop.`id_product` = p.`id_product` AND image_shop.cover=1 AND image_shop.id_shop=' . (int) $this->context->shop->id . ')
				LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il
					ON (image_shop.`id_image` = il.`id_image`
					AND il.`id_lang` = ' . (int) $idLang . ')
				LEFT JOIN `' . _DB_PREFIX_ . 'manufacturer` m
					ON m.`id_manufacturer` = p.`id_manufacturer`
				WHERE product_shop.`id_shop` = ' . (int) $this->context->shop->id . '
					AND p.`id_product` = ' . (int) $id_product
                    . ($active ? ' AND product_shop.`active` = 1' : '')
                    . ($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '');
        
        
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql, true, false);

        if (!$result) {
            return array();
        }
        
        return Product::getProductProperties($idLang, $result);
        
        
    }
    
    
    public function decryptPass($encrypted_pass) {
        
        $cipher = "aes-128-gcm";
        if (in_array($cipher, openssl_get_cipher_methods()))
        {
            $original_pass = openssl_decrypt($encrypted_pass, $cipher, _COOKIE_IV_, $options=0, utf8_decode(Configuration::get('AQURATE_PASS_IV')), utf8_decode(Configuration::get('AQURATE_PASS_TAG')));
            if (!$original_pass) return $encrypted_pass;
            return $original_pass;
        }
        
        return false;
        
    }
    
    public function encryptPass($password) {
        
        $cipher = "aes-128-gcm";
        if (in_array($cipher, openssl_get_cipher_methods()))
        {
            $ivlen = openssl_cipher_iv_length($cipher);
            $iv = openssl_random_pseudo_bytes($ivlen);
            $ciphertext = openssl_encrypt($password, $cipher, _COOKIE_IV_, $options=0, $iv, $tag);
            Configuration::updateValue('AQURATE_PASS_IV', utf8_encode($iv));
            Configuration::updateValue('AQURATE_PASS_TAG', utf8_encode($tag));
            if (!$ciphertext) return $password;
            return $ciphertext;
        }
        
         return false;
        
    }
    
    
    
    public function addConnectionLog($action, $log) {
        
        Db::getInstance()->Execute("INSERT INTO "._DB_PREFIX_."aqurate_api_logs (action, log, date_add) VALUES ('".pSQL($action)."','".pSQL($log)."', NOW())");
        
    }
    
    
    public function getAPICallHeaders() {
        
    
        $aqurate_module = Module::getInstanceByName('aqurate');
        
        $module_config = array();
        $module_config['widgets']['pdp'] = array('similar' => (bool)Configuration::get('AQURATE_SIMILAR_PRODUCT'), 'cross-sell' => (bool)Configuration::get('AQURATE_CROSSSELLING_PRODUCT'), 'substitutes' => (bool)Configuration::get('AQURATE_SUBSTITUTE_PRODUCT'));
        $module_config['widgets']['cart'] = array('similar' => (bool)Configuration::get('AQURATE_SIMILAR_CART'), 'cross-sell' => (bool)Configuration::get('AQURATE_CROSSSELLING_CART'));
        $module_config['widgets']['popup'] = array('similar' => (bool)Configuration::get('AQURATE_SIMILAR_CARTPOPUP'), 'cross-sell' => (bool)Configuration::get('AQURATE_CROSSSELLING_CARTPOPUP'));
        
        
        $headers = array(
            'X-Auth-Token: '.Configuration::get('AQURATE_TOKEN'),
            'integration-version: prestashop-'._PS_VERSION_,
            'aqurate-module-version: '.$aqurate_module->aqurate_version,
            'module-config: '.json_encode($module_config),
        );
        
        return $headers;
        
        
    }
    

}
