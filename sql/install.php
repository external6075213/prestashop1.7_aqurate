<?php
/**
* NOTICE OF LICENSE
*
* Aqurate - Integration for PrestaShop.
* @author MACHINE LEARNING SOLUTIONS SRL
* @copyright Copyright (C) 2023 MACHINE LEARNING SOLUTIONS SRL
* @license https://opensource.org/license/mit The MIT License (MIT) 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
* THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
$sql = array();


$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'aqurate_crossselling` (
`id_product` int(11) NOT NULL,
`id_cross_product` int(11) NOT NULL,
`position` int(1) NULL,
`date_upd` datetime NULL,
PRIMARY KEY (`id_product`,`id_cross_product`)) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'aqurate_substitute` (
`id_product` int(11) NOT NULL,
`id_substitute_product` int(11) NOT NULL,
`position` int(1) NULL,
`date_upd` datetime NULL,
PRIMARY KEY (`id_product`,`id_substitute_product`)) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'aqurate_similar` (
`id_product` int(11) NOT NULL,
`id_similar_product` int(11) NOT NULL,
`position` int(1) NULL,
`date_upd` datetime NULL,
PRIMARY KEY (`id_product`,`id_similar_product`)) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';


$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'aqurate_api_logs` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`id_product` int(11) NULL,
`action` varchar(200) NULL,
`log` text NULL,
`date_add` datetime NULL, 
PRIMARY KEY (`id`)) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';


$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'aqurate_cron_jobs` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`type` varchar(100) NULL,
`action` varchar(200) NULL,
`date_add` datetime NULL, 
PRIMARY KEY (`id`)) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';



foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
