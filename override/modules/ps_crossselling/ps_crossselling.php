<?php
/**
* NOTICE OF LICENSE
*
* Aqurate - Integration for PrestaShop.
* @author MACHINE LEARNING SOLUTIONS SRL
* @copyright Copyright (C) 2023 MACHINE LEARNING SOLUTIONS SRL
* @license https://opensource.org/license/mit The MIT License (MIT) 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
* THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;
use PrestaShop\PrestaShop\Core\Module\WidgetInterface;
use PrestaShop\PrestaShop\Core\Product\ProductListingPresenter;


class Ps_CrosssellingOverride extends Ps_Crossselling
{

    public function renderWidget($hookName, array $configuration) {
        
        if (!Module::isInstalled('aqurate') or !Module::isEnabled('aqurate')) return parent::renderWidget($hookName, $configuration);
        
        $aqurate_module = Module::getInstanceByName('aqurate');
        if (!$aqurate_module) return parent::renderWidget($hookName, $configuration);
        
        $product_ids = false;
        
        if ($hookName == 'displayCartModalContent') {
            
            if (!Configuration::get('AQURATE_CROSSSELLING_CARTPOPUP') && !Configuration::get('AQURATE_SIMILAR_CARTPOPUP')) return parent::renderWidget($hookName, $configuration);
            
            if (!isset($configuration['product']['id_product'])) return parent::renderWidget($hookName, $configuration); 
            
            $product_ids = $this->getCartModalProds($aqurate_module, (int)$configuration['product']['id_product']);
            
        }
        else if (Tools::getValue('controller') == 'product') {
            
            if (!Configuration::get('AQURATE_CROSSSELLING_PRODUCT')) return parent::renderWidget($hookName, $configuration);
            
            $product_ids = $this->getProductPageProds($aqurate_module);
            
        }
        else if (Tools::getValue('controller') == 'cart') {
            
            if (!Configuration::get('AQURATE_CROSSSELLING_CART') && !Configuration::get('AQURATE_SIMILAR_CART')) return parent::renderWidget($hookName, $configuration);
            
            $product_ids = $this->getCartPageProds($aqurate_module);
        }
        
        if (!$product_ids) return parent::renderWidget($hookName, $configuration);
        

        $products = $this->convertToPrestaProducts($product_ids, $aqurate_module);
        
        if (!empty($products)) {
            
            $this->smarty->assign(array('products' => $products));
            
            $tpl_content = $this->fetch('module:ps_crossselling/views/templates/hook/ps_crossselling.tpl');
            
            $tpl_content = str_replace('<a class="', '<a class="aqurate-personalize ', $tpl_content);
            $tpl_content = str_replace('<a href=', '<a class="aqurate-personalize" href=', $tpl_content);
            
            return $tpl_content;
           
         } else return parent::renderWidget($hookName, $configuration);
        
        
        
    }
    
    
    public function getProductPageProds($aqurate_module) {
        
        if (!Tools::getValue('id_product')) return false;
        
        return $aqurate_module->clasa->getCrossSellingProducts(Tools::getValue('id_product'));
           
    }
    
    public function getCartModalProds($aqurate_module, $id_product) {
        
        if (Configuration::get('AQURATE_CROSSSELLING_CARTPOPUP'))
            return $aqurate_module->clasa->getCrossSellingProducts($id_product);
        else if (Configuration::get('AQURATE_SIMILAR_CARTPOPUP'))
            return $aqurate_module->clasa->getSimilarProducts($id_product);
           
    }
    
    
    public function getCartPageProds($aqurate_module) {
        
        $highest_value_product_id = 0;
        $highest_value_product_price = 0;
        
        $cart = new Cart(Context::getContext()->cart->id);
        if (!isset($cart->id)) return false;
        
        foreach ($cart->getProducts() as $p) {
            
            $price = Product::getPriceStatic($p['id_product'], true, false, 4, NULL, false, true, 1, false, null, null, null);
            if ($price > $highest_value_product_price) {
                $highest_value_product_id = $p['id_product'];
                $highest_value_product_price = $price;
            }
    
        }
        
        if ($highest_value_product_id == 0) return false;
        
        if (Configuration::get('AQURATE_CROSSSELLING_CART'))
            return $aqurate_module->clasa->getCrossSellingProducts($highest_value_product_id);
        else if (Configuration::get('AQURATE_SIMILAR_CART'))
            return $aqurate_module->clasa->getSimilarProducts($highest_value_product_id);
          
        
    }

    
     public function convertToPrestaProducts($product_ids, $aqurate_module) {
       
        $products = array();
        foreach ($product_ids as $id_product)
            $products[] = array('id_product' => $id_product);
            
        
        $assembler = new ProductAssembler($this->context);

        $presenterFactory = new ProductPresenterFactory($this->context);
        $presentationSettings = $presenterFactory->getPresentationSettings();
        $presenter = new ProductListingPresenter(
            new ImageRetriever(
                $this->context->link
            ),
            $this->context->link,
            new PriceFormatter(),
            new ProductColorsRetriever(),
            $this->context->getTranslator()
        );

        $products_for_template = [];

        foreach ($products as $p) 
          if (isset($p['id_product']) && $p['id_product'] > 0) {
            $rawProduct = $aqurate_module->clasa->getProductDetailsAsArray($p['id_product']);
            if (!empty($rawProduct)) 
                $products_for_template[] = $presenter->present(
                    $presentationSettings,
                    $assembler->assembleProduct($rawProduct),
                    $this->context->language
                );
              
        }

        return $products_for_template;
        
    }
    
    

    
    
    
}