<?php
/**
* NOTICE OF LICENSE
*
* Aqurate - Integration for PrestaShop.
* @author MACHINE LEARNING SOLUTIONS SRL
* @copyright Copyright (C) 2023 MACHINE LEARNING SOLUTIONS SRL
* @license https://opensource.org/license/mit The MIT License (MIT) 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
* THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;
use PrestaShop\PrestaShop\Core\Module\WidgetInterface;
use PrestaShop\PrestaShop\Core\Product\ProductListingPresenter;

class Ps_CategoryproductsOverride extends Ps_Categoryproducts
{

    public function renderWidget($hookName = null, array $configuration = array()) {
        
        if ((Tools::getValue('controller') != 'product') or !Module::isInstalled('aqurate') or !Module::isEnabled('aqurate')) return parent::renderWidget($hookName, $configuration);
        
        if (!Configuration::get('AQURATE_SUBSTITUTE_PRODUCT') && !Configuration::get('AQURATE_SIMILAR_PRODUCT')) return parent::renderWidget($hookName, $configuration);
        
        $aqurate_module = Module::getInstanceByName('aqurate');

        if (!$aqurate_module) return parent::renderWidget($hookName, $configuration);
        
        if (Configuration::get('AQURATE_SUBSTITUTE_PRODUCT'))
           $product_ids = $aqurate_module->clasa->getSubstituteProducts(Tools::getValue('id_product'));
        else if (Configuration::get('AQURATE_SIMILAR_PRODUCT'))
            $product_ids = $aqurate_module->clasa->getSimilarProducts(Tools::getValue('id_product'));
            
        if ($product_ids)
            $products = $this->convertToPrestaProducts($product_ids, $aqurate_module);
        else return parent::renderWidget($hookName, $configuration);
        
        if (!empty($products)) {
            $this->smarty->assign(array('products' => $products));

            $tpl_content = $this->fetch('module:ps_categoryproducts/views/templates/hook/ps_categoryproducts.tpl');
            
            $tpl_content = str_replace('<a class="', '<a class="aqurate-personalize ', $tpl_content);
            $tpl_content = str_replace('<a href=', '<a class="aqurate-personalize" href=', $tpl_content);
            
            return $tpl_content;
            
            
         } else return parent::renderWidget($hookName, $configuration);
         
        
    }
    
    
    public function convertToPrestaProducts($product_ids, $aqurate_module) {
        
        $products = array();
        foreach ($product_ids as $id_product)
            $products[] = array('id_product' => $id_product);
            
        
        $assembler = new ProductAssembler($this->context);

        $presenterFactory = new ProductPresenterFactory($this->context);
        $presentationSettings = $presenterFactory->getPresentationSettings();
        $presenter = new ProductListingPresenter(
            new ImageRetriever(
                $this->context->link
            ),
            $this->context->link,
            new PriceFormatter(),
            new ProductColorsRetriever(),
            $this->context->getTranslator()
        );

        $products_for_template = [];

        foreach ($products as $p) 
          if (isset($p['id_product']) && $p['id_product'] > 0) {
            $rawProduct = $aqurate_module->clasa->getProductDetailsAsArray($p['id_product']);
            if (!empty($rawProduct)) 
                $products_for_template[] = $presenter->present(
                    $presentationSettings,
                    $assembler->assembleProduct($rawProduct),
                    $this->context->language
                );
              
        }

        return $products_for_template;
        
    }
    
    

    
    
    
}